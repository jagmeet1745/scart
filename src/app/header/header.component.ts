import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '../../../node_modules/@angular/router';
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private data = [{
    "name": "mango",
    "category": "fruit",
    "price": "80"
}, {
    "name": "orange",
    "category": "fruit",
    "price": "80"
}, {
    "name": "kiwi",
    "category": "fruit",
    "price": "20"
}, {
    "name": "banana",
    "category": "fruit",
    "price": "40"
}, {
    "name": "pomegranate",
    "category": "fruit",
    "price": "100"
}, {
    "name": "blackcurrant",
    "category": "fruit",
    "price": "1000"
}, {
    "name": "avocado",
    "category": "fruit",
    "price": "1000"
}, {
    "name": "grapes",
    "category": "fruit",
    "price": "50"
}, {
    "name": "strawberry",
    "category": "fruit",
    "price": "40"
}, {
    "name": "peach",
    "category": "fruit",
    "price": "60"
}, {
    "name": "broccoli",
    "category": "vegetable",
    "price": "300"
}, {
    "name": "cabbage",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "potato",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "carrot",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "tomato",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "turnip",
    "category": "vegetable",
    "price": "150"
}, {
    "name": "pumpkin",
    "category": "vegetable",
    "price": "300"
}, {
    "name": "onion",
    "category": "vegetable",
    "price": "200"
}, {
    "name": "mushroom",
    "category": "vegetable",
    "price": "500"
}, {
    "name": "capsicum",
    "category": "vegetable",
    "price": "200"
}, {
    "name": "oreo",
    "category": "biscuit",
    "price": "35"
}, {
    "name": "goodday",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "parleg",
    "category": "biscuit",
    "price": "10"
}, {
    "name": "tiger",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "crackjack",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "monaco",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "treat",
    "category": "biscuit",
    "price": "30"
}, {
    "name": "darkfantasy",
    "category": "biscuit",
    "price": "35"
}, {
    "name": "hidenseek",
    "category": "biscuit",
    "price": "50"
}, {
    "name": "bounce",
    "category": "biscuit",
    "price": "10"
}, {
    "name": "milk",
    "category": "diary",
    "price": "25"
}, {
    "name": "cheese",
    "category": "diary",
    "price": "100"
}, {
    "name": "curd",
    "category": "diary",
    "price": "25"
}, {
    "name": "butter",
    "category": "diary",
    "price": "30"
}, {
    "name": "icecream",
    "category": "diary",
    "price": "100"
}, {
    "name": "dessert",
    "category": "diary",
    "price": "100"
}, {
    "name": "custard",
    "category": "diary",
    "price": "250"
}, {
    "name": "milkshake",
    "category": "diary",
    "price": "100"
}, {
    "name": "yogurt",
    "category": "diary",
    "price": "100"
}, {
    "name": "tofu",
    "category": "diary",
    "price": "300"
}]

  constructor(private dataservice:DataService, private router:Router) { 
    
    

  }
  sendMessage(): void {
    // send message to subscribers via observable subject
    this.dataservice.sendMessage(this.data);
}
  ngOnInit() {
    this.sendMessage()
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  displayCategory(category:string){
    this.dataservice.setCategory(category);
    this.router.navigate(['/category',category])
  }
}
