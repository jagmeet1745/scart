import { Component, OnInit } from '@angular/core';
import { filterQueryId } from '../../../node_modules/@angular/core/src/view/util';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private keys;
  private data;
      subscription: Subscription;
  private message;
  constructor(private dataservice: DataService) {
    this.subscription = this.dataservice.getMessage().subscribe(res=>{console.log(res)})
  }
    
ngOnInit() {
  this.data = this.dataservice.getData();

  this.keys = Object.keys(this.data[0]);
  // console.log(this.keys)

}
cleardata(){
  this.data='';
}
}
