import { Injectable } from '@angular/core';
import { Subject, Observable } from '../../node_modules/rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    private data = [{
        "name": "mango",
        "category": "fruit",
        "price": "80"
    }, {
        "name": "orange",
        "category": "fruit",
        "price": "80"
    }, {
        "name": "kiwi",
        "category": "fruit",
        "price": "20"
    }, {
        "name": "banana",
        "category": "fruit",
        "price": "40"
    }, {
        "name": "pomegranate",
        "category": "fruit",
        "price": "100"
    }, {
        "name": "blackcurrant",
        "category": "fruit",
        "price": "1000"
    }, {
        "name": "avocado",
        "category": "fruit",
        "price": "1000"
    }, {
        "name": "grapes",
        "category": "fruit",
        "price": "50"
    }, {
        "name": "strawberry",
        "category": "fruit",
        "price": "40"
    }, {
        "name": "peach",
        "category": "fruit",
        "price": "60"
    }, {
        "name": "broccoli",
        "category": "vegetable",
        "price": "300"
    }, {
        "name": "cabbage",
        "category": "vegetable",
        "price": "100"
    }, {
        "name": "potato",
        "category": "vegetable",
        "price": "100"
    }, {
        "name": "carrot",
        "category": "vegetable",
        "price": "100"
    }, {
        "name": "tomato",
        "category": "vegetable",
        "price": "100"
    }, {
        "name": "turnip",
        "category": "vegetable",
        "price": "150"
    }, {
        "name": "pumpkin",
        "category": "vegetable",
        "price": "300"
    }, {
        "name": "onion",
        "category": "vegetable",
        "price": "200"
    }, {
        "name": "mushroom",
        "category": "vegetable",
        "price": "500"
    }, {
        "name": "capsicum",
        "category": "vegetable",
        "price": "200"
    }, {
        "name": "oreo",
        "category": "biscuit",
        "price": "35"
    }, {
        "name": "goodday",
        "category": "biscuit",
        "price": "20"
    }, {
        "name": "parleg",
        "category": "biscuit",
        "price": "10"
    }, {
        "name": "tiger",
        "category": "biscuit",
        "price": "20"
    }, {
        "name": "crackjack",
        "category": "biscuit",
        "price": "20"
    }, {
        "name": "monaco",
        "category": "biscuit",
        "price": "20"
    }, {
        "name": "treat",
        "category": "biscuit",
        "price": "30"
    }, {
        "name": "darkfantasy",
        "category": "biscuit",
        "price": "35"
    }, {
        "name": "hidenseek",
        "category": "biscuit",
        "price": "50"
    }, {
        "name": "bounce",
        "category": "biscuit",
        "price": "10"
    }, {
        "name": "milk",
        "category": "diary",
        "price": "25"
    }, {
        "name": "cheese",
        "category": "diary",
        "price": "100"
    }, {
        "name": "curd",
        "category": "diary",
        "price": "25"
    }, {
        "name": "butter",
        "category": "diary",
        "price": "30"
    }, {
        "name": "icecream",
        "category": "diary",
        "price": "100"
    }, {
        "name": "dessert",
        "category": "diary",
        "price": "100"
    }, {
        "name": "custard",
        "category": "diary",
        "price": "250"
    }, {
        "name": "milkshake",
        "category": "diary",
        "price": "100"
    }, {
        "name": "yogurt",
        "category": "diary",
        "price": "100"
    }, {
        "name": "tofu",
        "category": "diary",
        "price": "300"
    }]

    private subject = new Subject<any>();
    private categoryData = [];
    private key = []
    constructor() { }

    sendMessage(data:any) {
        this.subject.next(data);
    }
 
    clearMessage() {
        this.subject.next();
    }
 
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    
    getData() {
        return this.data;
    }
    setCategory(category) {
        this.categoryData=[];
        this.data.forEach((item) => {
            if (category === item.category) {
                this.categoryData.push(item)
            }
        })
    }

    getCategory() {
        console.log(this.categoryData)
        return this.categoryData
    }
}
