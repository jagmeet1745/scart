import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-categorywise',
  templateUrl: './categorywise.component.html',
  styleUrls: ['./categorywise.component.css']
})
export class CategorywiseComponent implements OnInit {

  constructor(private dataservice:DataService,private route: ActivatedRoute) { }
private data;
private keys;
  ngOnInit() {
this.data=this.dataservice.getCategory()
console.log(this.data)
this.keys=Object.keys(this.data[0]);
  }

}
