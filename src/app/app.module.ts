import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { DataService } from './data.service';
import { CategorywiseComponent } from './categorywise/categorywise.component';
const routes: Routes = [{

  path: 'category/:category',
  component: CategorywiseComponent,

},
{

  path: '',
  component: HomeComponent,

}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    CategorywiseComponent,
    ],
  imports: [
    BrowserModule, RouterModule.forRoot(routes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
